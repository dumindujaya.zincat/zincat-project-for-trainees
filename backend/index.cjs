const express = require("express");
const cors = require("cors");
const mysql = require("mysql2");

const app = express();

app.use(express.json());
app.use(cors());

require("dotenv").config();

const pool = mysql.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PWD,
  database: process.env.DB_NAME,
  port: process.env.DB_PORT,
  waitForConnections: process.env.DB_WAITFORCONNECTIONS,
  connectionLimit: process.env.DB_POOLLIMIT,
  queueLimit: process.env.DB_QUEUELIMIT,
});

const generateCodeForCustomer = (schema) => {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (err, connection) {
      if (err) {
        console.log("connection failed!");
        reject(err);
        return;
      }
      connection.execute(
        `SELECT code from ${schema} ORDER BY code DESC LIMIT 1;`,
        (err, result) => {
          if (err) {
            connection.release();
            reject(err);
          }
          connection.release();
          //   previous code
          if (result[0]) {
            let prev = Object.values(result[0])[0];
            let newCode = generateNextCode(prev);
            resolve(newCode);
          } else {
            resolve("C001");
          }
        }
      );
    });
  });
};

let generateNextCode = (code) => {
  let prefix = code.slice(0, 1);
  let suffix = code.slice(1);
  return prefix + padWithLeadingZeros(parseInt(suffix) + 1, suffix.length);
};

function padWithLeadingZeros(num, totalLength) {
  return String(num).padStart(totalLength, "0");
}

const addUser = (customer) => {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (err, connection) {
      if (err) {
        console.log("connection failed!");
        reject(err);
      }
      connection.beginTransaction(async function (err) {
        if (err) {
          //Transaction Error (Rollback and release connection)
          connection.rollback(function () {
            connection.release();
            console.log("transaction failed!");
            reject();
          });
        } else {
          // query goes here
          //   first of all, genereate code for the customer
          let code = await generateCodeForCustomer("customer");
          let {
            id,
            type,
            ref_no,
            name,
            company,
            nic,
            address,
            mobile,
            email,
            country,
            city,
            gender,
            documents,
            contactPerson,
            log,
          } = customer;
          connection.execute(
            "INSERT INTO customer VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
            [
              id,
              type,
              code,
              ref_no,
              name,
              company,
              nic,
              address,
              mobile,
              email,
              country,
              city,
              gender,
              true,
            ],
            function (err, results) {
              if (err) {
                //Query Error (Rollback and release connection)
                connection.rollback(function () {
                  connection.release();
                  console.log("transaction failed while saving customer!");
                  reject(err);
                });
              } else {
                documents.forEach((doc) => {
                  let { name, type, data } = doc;
                  connection.execute(
                    "INSERT INTO customer_document (name, type, data, customer) VALUES(?,?,?,?)",
                    [name, type, data, customer.id],
                    (err, result) => {
                      if (err) {
                        connection.rollback(function () {
                          connection.release();
                          console.log(
                            "transaction failed while saving a document!"
                          );
                          reject(err);
                        });
                      }
                    }
                  );
                });
                contactPerson.forEach((contact) => {
                  let { id, name, designation, mobile, email } = contact;
                  // search for the person and save accordingly
                  connection.execute(
                    "SELECT COUNT(id) FROM contact_person WHERE email=?",
                    [email],
                    (err, result) => {
                      if (err) {
                        connection.rollback(function () {
                          connection.release();
                          console.log(
                            "transaction failed while searching for a contact person!"
                          );
                          reject(err);
                        });
                      }
                      // if doesn't exists
                      if (Object.keys(result[0])[0]) {
                        connection.execute(
                          "INSERT INTO contact_person VALUES(?,?,?,?,?)",
                          [id, name, designation, mobile, email],
                          (err, result) => {
                            if (err) {
                              connection.rollback(function () {
                                connection.release();
                                console.log(
                                  "transaction failed while saving a contact person!"
                                );
                                reject(err);
                              });
                            }
                          }
                        );
                      }
                      // always save in the join table
                      connection.execute(
                        "INSERT INTO customer_contact_person VALUES(?,?)",
                        [customer.id, id],
                        (err, result) => {
                          if (err) {
                            connection.rollback(function () {
                              connection.release();
                              console.log(
                                "transaction failed while saving a contact person join!"
                              );
                              reject(err);
                            });
                          }
                        }
                      );
                    }
                  );
                });

                let { user, type } = log;
                connection.execute(
                  "INSERT INTO log (user,type,customer) VALUES(?,?,?)",
                  [user, type, code],
                  (err, result) => {
                    if (err) {
                      connection.rollback(function () {
                        connection.release();
                        console.log("transaction failed while saving a log!");
                        reject(err);
                      });
                    } else {
                      connection.commit(function (err) {
                        if (err) {
                          connection.rollback(function () {
                            connection.release();
                            console.log("error while commiting!");
                            reject(err);
                          });
                        } else {
                          connection.release();
                          //Success
                          console.log("customer saved!");
                          resolve("successful");
                        }
                      });
                    }
                  }
                );
              }
            }
          );
        }
      });
    });
  });
};

const findUser = (param) => {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (err, connection) {
      if (err) {
        console.log("connection failed!");
        reject(err);
        return;
      }
      connection.execute(
        `SELECT c.code, c.name, c.city, c.gender, ct.type FROM customer c LEFT JOIN customer_type ct ON c.type = ct.id WHERE c.code LIKE '%${param}%' OR c.name LIKE '%${param}%' OR c.city LIKE '%${param}%' OR c.gender LIKE '%${param}%' OR ct.type LIKE '%${param}%';`,
        (err, result) => {
          if (err) {
            connection.release();
            reject(err);
          }
          resolve(result);
        }
      );
    });
  });
};

const removeUser = (log) => {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (err, connection) {
      if (err) {
        console.log("connection failed!");
        reject(err);
      }
      connection.beginTransaction(function (err) {
        if (err) {
          //Transaction Error (Rollback and release connection)
          connection.rollback(function () {
            connection.release();
            console.log("transaction failed!");
            reject();
          });
        } else {
          // update user fields
          connection.execute(
            "UPDATE customer SET alive=0 WHERE code=?",
            [log.customer],
            (err, result) => {
              if (err) {
                connection.rollback(function () {
                  connection.release();
                  console.log("transaction failed!");
                  reject(err);
                });
              }
              //   update logs
              connection.execute(
                "INSERT INTO log (user, type, customer) VALUES (?,?,?)",
                [log.user, log.type, log.customer],
                (err, result) => {
                  if (err) {
                    connection.rollback(function () {
                      connection.release();
                      console.log("transaction failed!");
                      reject(err);
                    });
                  }
                  connection.commit(function (err) {
                    if (err) {
                      connection.rollback(function () {
                        connection.release();
                        console.log("error while commiting!");
                        reject(err);
                      });
                    } else {
                      connection.release();
                      console.log("customer deleted!");
                      //Success
                      resolve(result);
                    }
                  });
                }
              );
            }
          );
        }
      });
    });
  });
};

const getAllUsers = () => {
  return new Promise((resolve, reject) => {
    pool.getConnection(function (err, connection) {
      if (err) {
        console.log("connection failed!");
        reject(err);
      }
      connection.execute(
        "SELECT c.code, c.name, c.city, c.gender, ct.type, c.country, c.mobile FROM customer c LEFT JOIN customer_type ct ON c.type = ct.id;",
        (err, result) => {
          if (err) {
            connection.release();
            reject(err);
          }
          resolve(result);
        }
      );
    });
  });
};

app.listen(process.env.SERVER_PORT, () => {
  console.log(`app is listening on port ${process.env.SERVER_PORT}`);
});

app.get("/getAll", (req, res) => {
  getAllUsers()
    .then((response) => {
      res.status(200);
      res.send(response);
    })
    .catch((err) => {
      console.log(err);
      res.send(err);
    });
});

app.post("/save", (req, res) => {
  addUser(req.body)
    .then((response) => {
      res.status(200);
      res.send(response);
    })
    .catch((err) => {
      console.log(err);
      res.send(err);
    });
});

app.get("/find/:idf", (req, res) => {
  findUser(req.params.idf)
    .then((respones) => {
      res.status(200);
      res.send(respones);
    })
    .catch((err) => {
      console.log(err);
      res.send(err);
    });
});

app.delete("/remove/", (req, res) => {
  removeUser(req.body)
    .then((respones) => {
      res.status(200);
      res.send(respones);
    })
    .catch((err) => {
      console.log(err);
      res.send(err);
    });
});
