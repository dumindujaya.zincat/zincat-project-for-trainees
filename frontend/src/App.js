import "./App.css";
import { Layout, Table } from "antd";
import { Input, Button } from "antd";
import axios from "axios";
import {
  DeleteOutlined,
  DownloadOutlined,
  EditOutlined,
  EyeOutlined,
} from "@ant-design/icons";
import { useEffect, useState } from "react";
const { Header, Content, Footer } = Layout;
const { Search } = Input;

function App() {
  const BASE_URL = "http://localhost:3600";
  const [data, setData] = useState([]);
  const [user, setUser] = useState({});


  useEffect(() => {
    // on refresh.... fetch customers
    axios
      .get(`${BASE_URL}/getAll`)
      .then((res) => {
        setData(res.data);
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  });

  const deletUser = (user) => { 
    axios
      .get(`${BASE_URL}/remove`)
      .then((res) => {
        setData(res.data);
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
   }

  const columns = [
    {
      title: "Code",
      dataIndex: "code",
      key: "code",
    },
    {
      key: "type",
      title: "Type",
      dataIndex: "type",
    },
    {
      key: "name",
      title: "Name",
      dataIndex: "name",
    },
    {
      key: "mobile",
      title: "Mobile",
      dataIndex: "mobile",
    },
    {
      key: "country",
      title: "Country",
      dataIndex: "country",
    },
    {
      key: "city",
      title: "City",
      dataIndex: "city",
    },
    {
      key: "gender",
      title: "Gender",
      dataIndex: "gender",
    },
    {
      key: "actions",
      title: "Actions",
      render: (record) => {
        return (
          <>
            <EyeOutlined
              onClick={() => {
                // onEditStudent(record);
              }}
              style={{ color: "green", marginLeft: 12 }}
            />
            <EditOutlined
              onClick={() => {
                // onEditStudent(record);
              }}
              style={{ marginLeft: 12 }}
            />
            <DeleteOutlined
              onClick={() => {
                // handleDelete();
              }}
              style={{ color: "red", marginLeft: 12 }}
            />
          </>
        );
      },
    },
  ];

  return (
    <Layout>
      <Header className="d-flex align-items-center justify-content-center bg-light">
        <Search className="w-25" />
        <div>
          {/* <Button type="primary" shape="round" icon={<DownloadOutlined />} /> */}
        </div>
      </Header>
      <Content>{data && <Table dataSource={data} columns={columns} />}</Content>
      <Footer></Footer>
    </Layout>
  );
}

export default App;
